---
layout: page
title: About
permalink: /about/
---

This is intended to be a playground.

If you want to see your edits in the proper way, you must install and use [jekyll](https://jekyllrb.com/docs/home/).

Installing git:

* On windows: [git for windows](https://gitforwindows.org/)
* On linux `apt-get install git git-gui` or something similar on redhat, centos and such

Overview of the structure
---------------------------

![overview]({{ site.baseurl}}/assets/repo-relations.png)


Part I: How to contribute to this/any gitlab project
------------------------------

1. Go to [the project](https://gitlab.com/{{ site.gitlab_username }}/git-demo)

2. Click fork project

3. Generate SSH key/connect SSH key ([Guide](https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair))

4. `git clone git@gitlab.com:<your user>/git-demo`

5. Edit project (e.g. add an .md file)

6. `git add` changes

7. `git commit` and give a proper description

8. Repeat 4-6

9. `git push` to send the changes to gitlab.

10. Go to `https://gitlab.com/<your user>/git-demo`

11. Click create pull request

12. Write some nice description

13. Wait for {{site.gitlab_username}} to approve and merge the update


Part II: Synchronizing with *upstream*
------------------------------

Updates to upstream will not automatically get into the fork.

1. Create a new *remote* repository reference (following [this](https://help.github.com/articles/configuring-a-remote-for-a-fork/) quide)

   `git remote add upstream https://gitlab.com/{{site.gitlab_username}}/git-demo.git`

2. Sync with upstream (see [this](https://help.github.com/articles/syncing-a-fork/) for details)

   `git pull upstream master`

3. Resolve any issues

4. `git push` to send it to your own repository


Part III: more...
-----------------

We have not talked about [branches](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) even though they are very fundamental.

Notice that we have both *master* and *gh-pages* branch. See [doc](https://pages.github.com/) for reasons.
